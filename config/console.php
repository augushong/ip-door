<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------

use app\common\command\admin\Version;
use app\common\command\admin\ResetPassword;
use app\common\command\Door;
use app\common\command\Proxy;
use app\common\command\proxy\Client;
use app\common\command\proxy\Server;
use app\common\command\Timer;

return [
    // 指令定义
    'commands' => [
        'curd'      => 'app\common\command\Curd',
        'node'      => 'app\common\command\Node',
        'OssStatic' => 'app\common\command\OssStatic',
        ResetPassword::class,
        Timer::class,
        Version::class,
        Door::class,
        Client::class,
        Server::class
    ],
];
