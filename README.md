# ip-door IP门禁

#### 介绍
ip端口转发，只允许指定的ip转发。

比如监听：8000端口，转发到8010端口。

并且内置多种IP白名单过滤规则。

可以将部分应用服务隐藏起来，只有部分设备可以成功链接。

#### 功能特性

- 动态白名单认证
- 面板设置转发规则
- 支持白名单模式、黑名单模式、混合模式
- 支持流量统计
- 支持拦截次数统计
- 支持生成命令iptables代理转发至ip-door
- 支持反向代理至集群（平均负载、随机负载）


#### 软件架构
这是一个使用workerman实现的IP转发拦截器。管理面板使用ulthon_admin实现。

#### 安装方式

- 面板
项目本身是一个完整的网站，有一个完整的网站环境：



搭建好网站环境后：
```
设置配置文件，连接数据库（主要是连接数据库）
cp .example.env .env

安装数据库
php think migrate:run
php think seed:run

```

进入后台：
```
默认账号：admin 
默认密码：123456
```

> 关于面板的开发文档，参考网址：http://doc.ulthon.com/read/augushong/ulthon_admin/home.html

- 拦截客户端

客户端基于workerman实现，环境要求参考文档：https://www.workerman.net/doc/workerman/install/requirement.html

安装环境，参考workerman即可。

搭建好环境后：

```
设置配置文件，连接数据库，设置路由规则
cp .example.env .env
```

```
[DATABASE]
TYPE=mysql
HOSTNAME=
DATABASE=
USERNAME=
PASSWORD=
HOSTPORT=3306
CHARSET=utf8
DEBUG=true
FIELDS_CACHE=true
PREFIX=ul_

[DOOR]
CONFIG_UID=62e38abb3cf8a  // 来自面板，可以看后面的截图
PROCESS_COUNT=4             // 一个端口转发进程数量

```

#### 使用方式

- 面板
安装完面板后，可以通过首页的快捷按钮，进入页面。会自动获取当前设备的ip，加入到白名单中，默认通行。可以通过后台修改。

![](/public/static/readme/index.png)

- 客户端

> 客户端也需要配置.env

正确配置env之后，运行命令：
```
php think door start 临时运行

php think door start --mode d  后台守护进程

php think door restart 重新加载配置
```

- env配置

除了正常的ulthon_admin要求的env配置方式，主要有这样一个配置项：

```
[DOOR]
CONFIG_UID=62e38abb3cf8a
PROCESS_COUNT=4
```



#### 面板截图


![](/public/static/readme/ip.png)

![](/public/static/readme/ip_reject.png)

![](/public/static/readme/rules_preview.png)