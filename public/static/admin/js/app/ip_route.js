define(["jquery", "easy-admin"], function ($, ea) {


    var querys = {};

    querys.listen_id = ea.getQueryVariable('listen_id');

    var queryStr = $.param(querys);

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip_route/index',
        add_url: 'app.ip_route/add?' + queryStr,
        edit_url: 'app.ip_route/edit',
        delete_url: 'app.ip_route/delete',
        export_url: 'app.ip_route/export',
        modify_url: 'app.ip_route/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                modifyReload: false,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '创建时间' },
                    { field: 'listen_id', title: '监听', trueHide: true, defaultSearchValue: querys.listen_id, searchOp: '=' },
                    { field: 'route_ip', title: '转发IP' },
                    { field: 'route_port', title: '端口' },
                    { field: 'status', search: 'select', title: '状态', selectList: ['禁用', '放行'], templet: ea.table.switch, },
                    { field: 'route_type', search: 'select', selectList: ea.getDataBrage('select_list_route_type'), title: '转发类型' },
                    { field: 'comment', title: '备注' },
                    { width: 250, title: '操作', templet: ea.table.tool, fixed: 'right' },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});