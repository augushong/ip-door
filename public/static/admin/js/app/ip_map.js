define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip_map/index',
        add_url: 'app.ip_map/add',
        edit_url: 'app.ip_map/edit',
        delete_url: 'app.ip_map/delete',
        export_url: 'app.ip_map/export',
        modify_url: 'app.ip_map/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'create_time', title: '创建时间'},                    {field: 'ip', title: 'ip'},                    {field: 'in_buffer', title: '请求的流量'},                    {field: 'to_buffer', title: '返回的流量'},                    {field: 'comment', title: '备注'},                    {width: 250, title: '操作', templet: ea.table.tool , fixed:'right'},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});