define(["jquery", "easy-admin"], function ($, ea) {


    var querys = {};

    querys.config_id = ea.getQueryVariable('config_id');

    var queryStr = $.param(querys);

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip_listen/index',
        add_url: 'app.ip_listen/add?' + queryStr,
        edit_url: 'app.ip_listen/edit',
        delete_url: 'app.ip_listen/delete',
        export_url: 'app.ip_listen/export',
        modify_url: 'app.ip_listen/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                modifyReload: false,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '创建时间' },
                    { field: 'config_id', title: '配置', defaultSearchValue: querys.config_id, trueHide: true, searchOp: '=' },
                    { field: 'ipConfig.title', title: '配置' },
                    { field: 'protocol', search: 'select', selectList: ea.getDataBrage('select_list_protocol'), title: '协议' },
                    { field: 'listen_ip', title: '监听IP' },
                    { field: 'listen_port', title: '端口' },
                    { field: 'status', search: 'select', title: '状态', selectList: ['禁用', '放行'], templet: ea.table.switch, },
                    { field: 'listen_type', search: 'select', selectList: ea.getDataBrage('select_list_listen_type'), title: '监听类型' },

                    

                    {
                        width: 250, title: '操作', templet: ea.table.tool, fixed: 'right', operat: [
                            'edit',
                            [{
                                text: '转发管理',
                                url: ea.url('app.ip_route/index'),
                                method: 'open',
                                auth: 'add',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                                titleField: function (data) {
                                    return '-监听：' + data.protocol + '://' + data.listen_ip + ':[' + data.listen_port + ']'
                                },
                                field: function (url, data, option) {
                                    return url + '?' + $.param({
                                        listen_id: data.id,

                                    })
                                }
                            }],
                            'delete'
                        ]
                    },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});