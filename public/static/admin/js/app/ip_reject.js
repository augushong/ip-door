define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip_reject/index',
        add_url: 'app.ip_reject/add',
        edit_url: 'app.ip_reject/edit',
        delete_url: 'app.ip_reject/delete',
        export_url: 'app.ip_reject/export',
        modify_url: 'app.ip_reject/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '首次请求时间', width: 200 },
                    { field: 'update_time', title: '最新请求时间', width: 200 },
                    { field: 'ip', title: 'ip', width: 200 },
                    { field: 'count', title: '拦截次数' },
                    { field: 'ip_info', title: 'IP信息', width: 200 },
                    { width: 250, title: '操作', templet: ea.table.tool, fixed: 'right' },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});