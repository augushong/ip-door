define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip/index',
        add_url: 'app.ip/add',
        edit_url: 'app.ip/edit',
        delete_url: 'app.ip/delete',
        export_url: 'app.ip/export',
        modify_url: 'app.ip/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                modifyReload: false,
                init: init,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'update_time', title: '连接时间', width: 200 },
                    { field: 'ip', title: 'ip', width: 200 },
                    { field: 'status', search: 'select', selectList: ea.getDataBrage('select_list_status'), title: '状态', templet: ea.table.switch, },
                    { field: 'in_buffer', title: '请求流量', templet: ea.table.bytes },
                    { field: 'to_buffer', title: '返回流量', templet: ea.table.bytes },
                    { field: 'ip_info', title: 'ip信息', width: 200 },
                    { field: 'desc', title: '备注', width: 200 },
                    { width: 250, title: '操作', templet: ea.table.tool, fixed: 'right' },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});