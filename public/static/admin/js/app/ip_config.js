define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'app.ip_config/index',
        add_url: 'app.ip_config/add',
        edit_url: 'app.ip_config/edit',
        delete_url: 'app.ip_config/delete',
        export_url: 'app.ip_config/export',
        modify_url: 'app.ip_config/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                modifyReload: false,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '创建时间' },
                    { field: 'title', title: '配置名称' },
                    { field: 'uid', title: '配置ID' },
                    { field: 'status', search: 'select', title: '状态', selectList: ['禁用', '放行'], templet: ea.table.switch, },
                    { field: 'comment', title: '备注' },
                    { field: 'load_count', title: '读取次数' },
                    { field: 'route_load_type', search: 'select', selectList: ea.getDataBrage('select_list_route_load_type'), title: '转发负载类型' },
                    {
                        width: 250, title: '操作', templet: ea.table.tool, fixed: 'right', operat: [
                            'edit',
                            [
                                {
                                    text: '规则管理',
                                    url: ea.url('app.ip_listen/index'),
                                    method: 'tab',
                                    auth: 'add',
                                    class: 'layui-btn layui-btn-xs layui-btn-success',
                                    field: function (url, data, option) {
                                        return url + '?' + $.param({
                                            config_id: data.id,

                                        })
                                    }
                                },
                                {
                                    text: '预览规则',
                                    url: ea.url('app.ip_config/read'),
                                    method: 'open',
                                    auth: 'add',
                                    class: 'layui-btn layui-btn-xs layui-btn-success',
                                    extend: 'data-full="true"',
                                    field: 'id'
                                }
                            ],
                            'delete',
                        ]
                    },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});