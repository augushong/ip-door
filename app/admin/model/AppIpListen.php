<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class AppIpListen extends TimeModel
{

    protected $name = "app_ip_listen";

    protected $deleteTime = "delete_time";


    public const SELECT_LIST_PROTOCOL = ['tcp' => 'TCP', 'udp' => 'UDP',];

    public const SELECT_LIST_LISTEN_TYPE = ['port' => '端口', 'range' => '端口范围', 'false_range' => '假端口范围',];

    public function ipConfig()
    {
        return $this->belongsTo(AppIpConfig::class, 'config_id');
    }
}
