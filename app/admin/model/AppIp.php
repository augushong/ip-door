<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\facade\Cache;
use think\facade\Log;

class AppIp extends TimeModel
{

    protected $name = "app_ip";

    protected $deleteTime = "delete_time";

    public static $withAppend = ['ip_info'];

    public const SELECT_LIST_STATUS = ['1' => '通行', '0' => '拦截',];

    public function getIpInfoAttr()
    {
        $ip = $this->getAttr('ip');

        $cache_key = md5($ip);

        $info = Cache::get($cache_key);

        if (is_null($info)) {
            $info = get_ip_info($ip);

            Cache::set($cache_key, $info, 86400*30);
        }

        return $info;
    }
}
