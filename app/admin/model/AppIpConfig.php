<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class AppIpConfig extends TimeModel
{

    protected $name = "app_ip_config";

    protected $deleteTime = "delete_time";


    public const SELECT_LIST_ROUTE_LOAD_TYPE = ['average' => '平均', 'random' => '随机',];


    public function getRouteLoadTypeTitleAttr()
    {
        $value = $this->getAttr('route_load_type');

        return static::SELECT_LIST_ROUTE_LOAD_TYPE[$value];
    }
}
