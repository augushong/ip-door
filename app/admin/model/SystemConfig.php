<?php



namespace app\admin\model;


use app\common\model\TimeModel;

class SystemConfig extends TimeModel
{
    protected $deleteTime = false;

    const LIST_RULE_TYPE = [
        'whitelist_only' => '白名单模式',
        'blacklist_only' => '黑名单模式',
        'allow_mixed' => '混合模式',
    ];

    const UNKNOWN_RULE_TYPE = [
        'allow' => '默认放行',
        'deny' => '默认拦截',
        'allow_record' => '默认放行并加入白名单',
        'deny_record' => '默认拦截并加入黑名单',
    ];
}
