<?php

namespace app\admin\controller\app;

use app\common\controller\AdminController;
use app\admin\service\annotation\ControllerAnnotation;
use app\admin\service\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="app_ip")
 */
class Ip extends AdminController
{

    protected $sort = [
        'status' => 'desc',
        'update_time' => 'desc',
        'to_buffer' => 'desc'
    ];

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AppIp();

        $this->assign('select_list_status', $this->model::SELECT_LIST_STATUS, true);
    }
}
