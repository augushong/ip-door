<?php

namespace app\admin\controller\app;

use app\common\controller\AdminController;
use app\admin\service\annotation\ControllerAnnotation;
use app\admin\service\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="app_ip_reject")
 */
class IpReject extends AdminController
{
    protected $sort = [
        'update_time' => 'desc'
    ];

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AppIpReject();
    }
}
