<?php

namespace app\admin\controller\app;

use app\common\controller\AdminController;
use app\admin\service\annotation\ControllerAnnotation;
use app\admin\service\annotation\NodeAnotation;
use think\App;
use think\validate\ValidateRule;

/**
 * @ControllerAnnotation(title="app_ip_listen")
 */
class IpListen extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AppIpListen();

        $this->assign('select_list_protocol', $this->model::SELECT_LIST_PROTOCOL, true);

        $this->assign('select_list_listen_type', $this->model::SELECT_LIST_LISTEN_TYPE, true);

        if ($this->request->action() != 'modify') {
            $this->validateRule = [
                'listen_type' => ValidateRule::isRequire()->in(array_keys($this->model::SELECT_LIST_LISTEN_TYPE)),
                'listen_port' => ValidateRule::isRequire()->requireCallback(function ($port, $data) {
                    $listen_type = $data['listen_type'];

                    if ($listen_type == 'port') {
                        if (!is_numeric($port)) {
                            return '请输入纯数字';
                        }
                        if (!$port < 0 || $port > 65535) {

                            return '端口范围错误，应当大于0，小于65535';
                        }
                    } else if ($listen_type == 'range' || $listen_type == 'false_range') {
                        if (!preg_match('/\d+:\d+/', $port)) {
                            return '端口范围格式错误，格式：3100:3200';
                        }

                        $port_data = explode(':', $port);

                        if ($port_data[0] > $port_data[1]) {
                            return '端口范围格式错误，起始端口应当小于结束端口';
                        }

                        if (!$port_data[0] < 0 || $port_data[0] > 65535) {
                            return '起始端口范围错误，应当大于0，小于65535';
                        }
                        if (!$port_data[1] < 0 || $port_data[1] > 65535) {
                            return '结束端口范围错误，应当大于0，小于65535';
                        }
                    }

                    return true;
                })
            ];
        }
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->withJoin(['ipConfig'])
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin(['ipConfig'])
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }
}
