<?php

namespace app\common\worker\protocol;


use Workerman\Connection\TcpConnection;
use Workerman\Protocols\ProtocolInterface;

class Framex
{
    /**
     * Check the integrity of the package.
     *
     * @param string        $buffer
     * @param TcpConnection $connection
     * @return int
     */
    public static function input($buffer, TcpConnection $connection)
    {
        if (\strlen($buffer) < 4) {
            return 0;
        }
        $unpack_data = \unpack('Ntotal_length', $buffer);
        return $unpack_data['total_length'];
    }

    /**
     * Decode.
     *
     * @param string $buffer
     * @return string
     */
    public static function decode($buffer)
    {
        $data =  \substr($buffer, 4);

        $data = base64_decode($data);

        return $data;
    }

    /**
     * Encode.
     *
     * @param string $buffer
     * @return string
     */
    public static function encode($buffer)
    {
        $buffer = base64_encode($buffer);
        
        $total_length = 4 + \strlen($buffer);


        return \pack('N', $total_length) . $buffer;
    }
}
