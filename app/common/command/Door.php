<?php

declare(strict_types=1);

namespace app\common\command;

use app\admin\model\AppIp;
use app\admin\model\AppIpReject;
use app\common\service\DoorService;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Db;
use think\facade\Env;
use think\facade\Log;
use Workerman\Connection\AsyncTcpConnection;
use Workerman\Connection\TcpConnection;
use Workerman\Timer;
use Workerman\Worker;

class Door extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('door')
            ->addArgument('action', Argument::OPTIONAL, "start|stop|restart|reload|status|connections", 'start')
            ->addOption('mode', 'm', Option::VALUE_OPTIONAL, 'Run the workerman server in daemon mode.')
            ->setDescription('the door command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('door');


        $action = $input->getArgument('action');
        $mode = $input->getOption('mode');


        // 重新构造命令行参数,以便兼容workerman的命令
        global $argv;

        $argv = [];

        array_unshift($argv, 'think', $action);

        if ($action == 'restart') {
            $mode = 'd';
        }

        if ($mode == 'd') {
            $argv[] = '-d';
        } else if ($mode == 'g') {
            $argv[] = '-g';
        }

        $config_uid = Env::get('door.config_uid');

        $door_service = new DoorService($config_uid, $output);

        $door_service->initReportWorker();
        $door_service->initDoorWorker();

        Db::close();

        Worker::runAll();
    }
}
