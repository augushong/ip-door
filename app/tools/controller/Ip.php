<?php

declare(strict_types=1);

namespace app\tools\controller;

use app\admin\model\AppIp;
use app\common\controller\ToolsController;
use think\Request;

class Ip extends ToolsController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //

        $ip = $this->request->ip();

        $model_ip = AppIp::where('ip', $ip)->find();

        if (empty($model_ip)) {
            $model_ip = new AppIp();

            $model_ip->ip = $ip;
            $model_ip->status = 1;

            $model_ip->save();
        }


        return json_message('已记录下IP：' . $ip);
    }
}
