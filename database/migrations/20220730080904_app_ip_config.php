<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppIpConfig extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_ip_config')
            ->setComment('过滤规则')
            ->addColumn(Column::char('title')->setDefault('')->setComment('配置名称'))
            ->addColumn(Column::char('uid')->setDefault('')->setComment('配置ID'))
            ->addColumn(Column::char('comment')->setDefault('')->setComment('备注 {textarea}'))
            ->addColumn(Column::bigInteger('load_count')->setDefault(0)->setComment('读取次数'))
            ->addColumn(Column::char('route_load_type')->setLimit(20)->setDefault('')->setComment('转发负载类型 {radio} (average:平均,random:随机)'))
            ->addColumn(Column::tinyInteger('status')->setLimit(1)->setUnsigned()->setDefault(1)->setComment('状态 {radio} (0:禁用,1:启用,)'))
            ->addColumn(Column::integer('create_time')->setLimit(11)->setUnsigned()->setDefault(0)->setComment('创建时间'))
            ->addColumn(Column::integer('update_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addColumn(Column::integer('delete_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addIndex('delete_time')
            ->addIndex('uid')
            ->addIndex('status')
            ->create();
    }
}
