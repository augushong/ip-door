<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppIpMap extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_ip_map')
            ->setComment('IP对转发的流量统计')
            ->addColumn(Column::char('ip')->setLimit(200)->setDefault('')->setComment('客户端的IP'))
            ->addColumn(Column::bigInteger('in_buffer')->setDefault(0)->setComment('请求的流量'))
            ->addColumn(Column::bigInteger('to_buffer')->setDefault(0)->setComment('返回的流量'))
            ->addColumn(Column::char('comment')->setLimit(200)->setDefault('')->setComment('备注 {textarea}'))
            ->addColumn(Column::integer('create_time')->setLimit(11)->setUnsigned()->setDefault(0)->setComment('创建时间'))
            ->addColumn(Column::integer('update_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addColumn(Column::integer('delete_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addIndex('ip')
            ->addIndex('delete_time')
            ->create();
    }
}
