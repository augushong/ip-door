<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppIpListen extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_ip_listen')
            ->setComment('监听设置')
            ->addColumn(Column::bigInteger('config_id')->setDefault(0)->setComment('配置'))
            ->addColumn(Column::char('protocol')->setLimit(10)->setDefault('tcp')->setComment('协议 {radio} (tcp:TCP,udp:UDP)'))
            ->addColumn(Column::char('listen_ip')->setLimit(200)->setDefault('0.0.0.0')->setComment('监听IP'))
            ->addColumn(Column::char('listen_port')->setLimit(10)->setDefault('')->setComment('端口'))
            ->addColumn(Column::char('listen_type')->setLimit(20)->setDefault('port')->setComment('监听类型 {radio} (port:端口,range:端口范围,false_range:假端口范围)'))
            ->addColumn(Column::char('comment')->setDefault('')->setComment('备注 {textarea}'))
            ->addColumn(Column::tinyInteger('status')->setLimit(1)->setUnsigned()->setDefault(1)->setComment('状态 {radio} (0:禁用,1:启用,)'))
            ->addColumn(Column::integer('create_time')->setLimit(11)->setUnsigned()->setDefault(0)->setComment('创建时间'))
            ->addColumn(Column::integer('update_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addColumn(Column::integer('delete_time')->setLimit(11)->setUnsigned()->setDefault(0))
            ->addIndex('delete_time')
            ->addIndex('status')
            ->addIndex('config_id')
            ->create();
    }
}
